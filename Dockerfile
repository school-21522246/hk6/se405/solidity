FROM node:slim
ENV NODE_ENV production
EXPOSE 3000

WORKDIR /usr/src/app

COPY package.json /usr/src/app/package.json
COPY package-lock.json /usr/src/app/package-lock.json
RUN npm ci

COPY . /usr/src/app

RUN npm install pm2 -g
CMD pm2 start process.yml && tail -f /dev/null

